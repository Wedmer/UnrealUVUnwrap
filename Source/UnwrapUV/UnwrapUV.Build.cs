 // Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class UnwrapUV : ModuleRules
{
    public UnwrapUV(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        
        // directxmath uses some undefined macros
        bEnableUndefinedIdentifierWarnings = false;

        PublicIncludePaths.AddRange(
            new string[] {
				// ... add public include paths required here ...
			}
            );


        PrivateIncludePaths.AddRange(
            new string[] {
				// ... add other private include paths required here ...
			}
            );


        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
				// ... add other public dependencies that you statically link with here ...
			}
            );


        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Projects",
                "InputCore",
                "UnrealEd",
                "LevelEditor",
                "CoreUObject",
                "Engine",
                "Slate",
                "SlateCore",
                "RawMesh",
                "MeshUtilities"
				// ... add private dependencies that you statically link with here ...	
			}
            );


        DynamicallyLoadedModuleNames.AddRange(
            new string[]
            {
				// ... add any modules that your module loads dynamically here ...
			}
            );

        
        var ThirdParty = Path.GetFullPath(Path.Combine(ModuleDirectory, "..", "..", "ThirdParty"));
        {
            var UVAtlas = Path.Combine(ThirdParty, "UVAtlas");
            PrivateIncludePaths.Add(Path.Combine(UVAtlas, "include"));
            PublicLibraryPaths.Add(Path.Combine(UVAtlas, "bin", "x64"));
            PublicAdditionalLibraries.Add("UVAtlas.lib");

            var DirectXMesh = Path.Combine(ThirdParty, "DirectXMesh");
            PrivateIncludePaths.Add(Path.Combine(DirectXMesh, "include"));
            PublicLibraryPaths.Add(Path.Combine(DirectXMesh, "bin", "x64"));
            PublicAdditionalLibraries.Add("DirectXMesh.lib");
        }
        
    }
}
