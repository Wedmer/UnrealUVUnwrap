// Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

#include "UnwrapUV.h"
#include "UnwrapUVCommands.h"
#include "UVAtlasAdapter.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Core/Public/Misc/ScopedSlowTask.h"
#include "Editor/StaticMeshEditor/Public/StaticMeshEditorModule.h"
#include "Editor/StaticMeshEditor/Public/IStaticMeshEditor.h"
#include "Editor/UnrealEd/Public/ScopedTransaction.h"
#include "Runtime/Core/Public/Misc/MessageDialog.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "Modules/ModuleManager.h"

static const FName UnwrapUVTabName("UnwrapUV");

#define LOCTEXT_NAMESPACE "FUnwrapUVModule"
DEFINE_LOG_CATEGORY_STATIC(LogUnwrapUV, Log, All);

void FUnwrapUVModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	FUnwrapUVStyle::Initialize();
	FUnwrapUVStyle::ReloadTextures();

	FUnwrapUVCommands::Register();

	IStaticMeshEditorModule *StaticMeshEditorModule = &FModuleManager::LoadModuleChecked<IStaticMeshEditorModule>("StaticMeshEditor");
	{
		// We cannot use StaticMeshEditorModule->GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender)
		// because this has no knowledge of the asset being opened. We need to wire up the callback for every open asset,
		// which means opening the 

		if(GEditor){
			UAssetEditorSubsystem *uaes=GEditor->GetEditorSubsystem<UAssetEditorSubsystem>();
			if(uaes){
				uaes->OnAssetOpenedInEditor().AddRaw(this, &FUnwrapUVModule::HandleAssetOpenedInEditor);
				return;
			}
		}
		UE_LOG(LogUnwrapUV, Warning, TEXT("Failed to register callback."));
	}
}

void FUnwrapUVModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FUnwrapUVStyle::Shutdown();

	FUnwrapUVCommands::Unregister();
}

float MeshSurfaceArea(const FRawMesh &mesh)
{
	float result = 0;
	for (auto wedge = 0; wedge < mesh.WedgeIndices.Num(); wedge += 3)
	{
		auto edge1 = mesh.GetWedgePosition(wedge) - mesh.GetWedgePosition(wedge + 1);
		auto edge2 = mesh.GetWedgePosition(wedge) - mesh.GetWedgePosition(wedge + 2);
		result += (edge1^edge2).Size() / 2;
	}
	return result;
}

void FUnwrapUVModule::PluginButtonClickedFromStaticMeshEditor(IStaticMeshEditor *MeshEditor)
{
	// see FLevelOfDetailSettingsLayout::ApplyChanges for how we should be modifying the mesh
	auto StaticMesh = MeshEditor->GetStaticMesh();
	check(StaticMesh);

	// Note we do NOT do this, because RenderData can contain UV coords produced by Build Mesh.
	// These go away when we rebuild the mesh.
	//for (auto &LODModel : StaticMesh->RenderData->LODResources)
	//{
	//	auto TC = LODModel.VertexBuffer.GetNumTexCoords();
	//	MaxUVChannel = FMath::Max(MaxNumUVs, TC);
	//}

	int32 MaxUVChannel = 0;
	for (auto &SourceModel : StaticMesh->GetSourceModels())
	{
		// Ignore RenderData, since this can contain UV coordinates produced by the Mesh Build process
		// So they can go away when we rebuild the mesh
		FRawMesh RawMesh;
		SourceModel.RawMeshBulkData->LoadRawMesh(RawMesh);
		for (int32 UVChannel = 0; UVChannel < MAX_MESH_TEXTURE_COORDS; UVChannel++)
		{
			if (RawMesh.WedgeTexCoords[UVChannel].Num() && UVChannel > MaxUVChannel)
				MaxUVChannel = UVChannel;
		}
	}

	// Use first unoccupied UV index
	// can't use MeshEditor->GetNumUVChannels(), since this reports 1, even if there are no uvs.
	auto LightmapIndex = FMath::Min(MaxUVChannel + 1, MAX_MESH_TEXTURE_COORDS - 1);

	FText DialogText = FText::Format(
		LOCTEXT("UnwrappingMesh", "Unwrapping mesh {0} to UV channel {1}"),
		FText::FromString(StaticMesh->GetName()),
		LightmapIndex
	);
	{
		// Deduplicate vertices, unwrap the mesh, and set lightmap settings
		{
			// do we need something like FStaticMeshLightingInfoStatsPage::SetMappingMethodOnSelectedComponents
			StaticMesh->Modify();

			float LightMapResolution = 32;
			auto nLODs = StaticMesh->GetSourceModels().Num();

			FScopedSlowTask SlowTask(nLODs, DialogText);
			// Here comes the slow stuff
			SlowTask.MakeDialog(false);
			for (auto LOD = 0; LOD < nLODs; LOD++)
			{
				auto StatusCallback = [&](float Progress) {
					// SlowTask.EnterProgressFrame announces how much work we're *about* to do,
					// but the status callback knows how much we've *already done*.
					SlowTask.CompletedWork = LOD + Progress;
					SlowTask.EnterProgressFrame(0.f);
					return 0; // nonzero return code would abort the unwrapping.
				};
				// If using Mesh Reduction, we can have more LODs than SourceModels!
				// While StaticMesh->RenderData->LODResources[LOD] will be valid,
				// StaticMesh->SourceModels[LOD] can have an empty mesh.
				if (StaticMesh->GetSourceModel(LOD).RawMeshBulkData->IsEmpty())
					continue;

				auto &SourceModel = StaticMesh->GetSourceModel(LOD);
				FRawMesh RawMesh;
				SourceModel.RawMeshBulkData->LoadRawMesh(RawMesh);

				if (RawMesh.IsValidOrFixable())
				{
					// The lightmap areal resolution should be proportional to the surface area
					// Proportionality constant determined by experiment
					float ComputedResolution = FMath::Sqrt(MeshSurfaceArea(RawMesh))*.42f;
					LightMapResolution = FMath::Max(LightMapResolution, ComputedResolution);

					DeduplicateVertices(RawMesh);
					auto NewRawMesh = UVAtlasUnwrap(RawMesh, LightmapIndex, FMath::CeilToInt(LightMapResolution), StatusCallback);

					// Repack all islands. I'm not sure if UVAtlas packs them better
					SourceModel.BuildSettings.MinLightmapResolution = LightMapResolution;
					SourceModel.BuildSettings.bGenerateLightmapUVs = true;
					SourceModel.BuildSettings.SrcLightmapIndex = LightmapIndex;
					SourceModel.BuildSettings.DstLightmapIndex = LightmapIndex;

					SourceModel.RawMeshBulkData->SaveRawMesh(NewRawMesh);
				}
				else
				{
					FMessageDialog::Open(EAppMsgType::Ok,
						FText::Format(LOCTEXT("MeshWasBad", "Could not unwrap mesh {0} LOD {1}, since it was invalid."), FText::FromString(StaticMesh->GetName()), LOD));
				}
			}

			FProperty* LightMapResolutionProperty =  FindFProperty<FProperty>(UStaticMesh::StaticClass(), "LightMapResolution");
			StaticMesh->LightMapResolution = LightMapResolution;
			auto LightMapResolutionChanged = FPropertyChangedEvent(LightMapResolutionProperty, EPropertyChangeType::ValueSet);
			StaticMesh->PostEditChangeProperty(LightMapResolutionChanged);

			FProperty* LightMapCoordinateProperty =  FindFProperty<FProperty>(UStaticMesh::StaticClass(), "LightMapCoordinateIndex");
			StaticMesh->LightMapCoordinateIndex = LightmapIndex;
			auto LightMapCoordinateChanged = FPropertyChangedEvent(LightMapCoordinateProperty, EPropertyChangeType::ValueSet);
			StaticMesh->PostEditChangeProperty(LightMapCoordinateChanged); // Resets the Lighting GUID, rebuilds mesh, marks using components as needing light rebuilt
		}

		MeshEditor->RefreshTool();
	}
	StaticMesh->SetLightingGuid();
}

void FUnwrapUVModule::HandleAssetOpenedInEditor(UObject * Asset, IAssetEditorInstance *Editor)
{
	// use this instead of HandleAssetEditorOpened because it has access to the editor
	auto Mesh = Cast<UStaticMesh>(Asset);
	// This function runs on every opened asset, so the asset we have *might* not be a Static Mesh
	if (!Mesh)
		return;

	check(Editor->GetEditorName() == "StaticMeshEditor");  // i.e. == FStaticMeshEditor::GetToolkitFName()
	auto MeshEditor = StaticCast<IStaticMeshEditor *>(Editor); // This cast is not safe, which is why we check GetEditorName
	{
		auto StaticMeshEditorCommands = MakeShared<FUICommandList>();
		StaticMeshEditorCommands->MapAction(
			FUnwrapUVCommands::Get().UnwrapMeshFromStaticMeshEditor,
			FExecuteAction::CreateRaw(this, &FUnwrapUVModule::PluginButtonClickedFromStaticMeshEditor, MeshEditor),
			FCanExecuteAction());

		// Extend the Command section of the toolbar:	
		auto ToolbarExtender = MakeShared<FExtender>();
		ToolbarExtender->AddToolBarExtension("Command", EExtensionHook::After, StaticMeshEditorCommands, FToolBarExtensionDelegate::CreateRaw(this, &FUnwrapUVModule::AddStaticMeshEditorToolbarExtension));

		MeshEditor->AddToolbarExtender(ToolbarExtender);
		// No need to call MeshEditor->RegenerateMenusAndToolbars();
	}
}

void FUnwrapUVModule::AddStaticMeshEditorToolbarExtension(FToolBarBuilder& Builder)
{
	Builder.AddToolBarButton(FUnwrapUVCommands::Get().UnwrapMeshFromStaticMeshEditor);
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FUnwrapUVModule, UnwrapUV)