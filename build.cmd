set BUILD=%~1
set UE4=%~2
set PLUGIN=UnwrapUV
set runuat=%UE4%/build/batchfiles/runuat.bat

if "%UE4%"=="" (
  SET UE4=D:/Epic/ue/UE_4.25/Engine
)

if "%BUILD%"=="" (
  SET BUILD=%~dp0/../../../Artifacts/%PLUGIN%
)
echo Building into %BUILD%

call "%runuat%" BuildPlugin -Plugin="%~dp0/%PLUGIN%.uplugin" -Package="%BUILD%"

echo Unstaging files not intended for distribution

rmdir /S/Q "%BUILD%/Saved"
rmdir /S/Q "%BUILD%/Intermediate"
rmdir /S/Q "%BUILD%/Source/%PLUGIN%/Private"
rmdir /S/Q "%BUILD%/Source/%PLUGIN%/Public"