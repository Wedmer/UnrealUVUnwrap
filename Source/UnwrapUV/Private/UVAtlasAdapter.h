﻿// Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

#pragma once
#include "Windows/AllowWindowsPlatformTypes.h"
#pragma warning(push, 0) // nothing we can do about build warnings in a third party library 
#include "UVAtlas.h"
#include "DirectXMesh.h"
#pragma warning(pop)
#include "Windows/HideWindowsPlatformTypes.h"

#include "CoreMinimal.h"
#include "Runtime/RawMesh/Public/RawMesh.h"
namespace dx = DirectX;

/** Modifies the given mesh to merge vertices that are nearly identical */
void DeduplicateVertices(FRawMesh &Mesh)
{
	// This is a slow algorithm O(n^2) but not too bad. 300k Wedge Indices -> 34 seconds
	TArray<FVector> NewVertices;
	TArray<uint32> NewWedges;
	for (auto p = 0; p < Mesh.WedgeIndices.Num(); p++)
	{
		auto Position = Mesh.GetWedgePosition(p);
		auto VertexIndex = -1;

		for (auto OtherVertexIndex = 0; OtherVertexIndex < NewVertices.Num(); OtherVertexIndex++)
		{
			if (FVector::PointsAreSame(Position, NewVertices[OtherVertexIndex]))
			{
				VertexIndex = OtherVertexIndex;
				break;
			}
		}
		if (VertexIndex == -1)
		{
			NewWedges.Add(NewVertices.Add(Mesh.GetWedgePosition(p)));
		}
		else
		{
			NewWedges.Add(VertexIndex);
		}
	}

	Mesh.WedgeIndices = NewWedges;
	Mesh.VertexPositions = NewVertices;
}

/** Detects any pairs of adjacent triangles which form a rectangle
  * and marks them as a false adjacency so that unwrapping doesn't cut along that edge*/
TArray<uint32> QuadFalseAdjacency(const FRawMesh &Mesh, const TArray<uint32> Adjacency)
{
	TArray<uint32> FalseAdjacency;

	for (auto Wedge = 0; Wedge < Mesh.WedgeIndices.Num(); Wedge++)
	{
		FalseAdjacency.Add(-1);
	}
	for (auto Wedge = 0; Wedge < Mesh.WedgeIndices.Num(); Wedge++)
	{
		bool bFalseEdge;

		auto Face = Wedge / 3;
		auto AdjacentFace = Adjacency[Wedge];
		uint32 OtherWedge = -1;

		if (AdjacentFace == -1)
		{
			// this is a boundary edge
			continue;
		}
		else
		{
			OtherWedge = Adjacency[AdjacentFace * 3] == Face ? AdjacentFace * 3
				: Adjacency[AdjacentFace * 3 + 1] == Face ? AdjacentFace * 3 + 1
				: Adjacency[AdjacentFace * 3 + 2] == Face ? AdjacentFace * 3 + 2
				: -1;

			check(OtherWedge != -1);

			auto W0 = Mesh.WedgeIndices[Wedge];
			auto V1 = Mesh.WedgeIndices[AdjacentFace * 3 + (OtherWedge + 1) % 3];

			auto W1 = Mesh.WedgeIndices[Face * 3 + (Wedge + 1) % 3];
			auto V0 = Mesh.WedgeIndices[OtherWedge];
			// Do NOT assume (W0 == V1) and (W1 == V0), but W0 and V1 should have the same position in space
			check(FVector::PointsAreNear(Mesh.VertexPositions[W0], Mesh.VertexPositions[V1], THRESH_POINTS_ARE_NEAR));
			check(FVector::PointsAreNear(Mesh.VertexPositions[W1], Mesh.VertexPositions[V0], THRESH_POINTS_ARE_NEAR));

			auto W2 = Mesh.WedgeIndices[Face * 3 + (Wedge + 2) % 3];
			auto V2 = Mesh.WedgeIndices[AdjacentFace * 3 + (OtherWedge + 2) % 3];

			//WP2-<-WP1=VP0
			// |    /  |
			// v ↺ / ↺ ^
			// |  /    |
			//WP0=VP1->-VP2

			auto Side0 = Mesh.VertexPositions[W0] - Mesh.VertexPositions[W2];
			auto Side1 = Mesh.VertexPositions[W1] - Mesh.VertexPositions[W2];

			// We will call this a false edge if the two triangles form a rectangle
			// Two conditions for a rectangle: sides are orthogonal four corners form a parallelogram 
			if (!FVector::Orthogonal(Side0.GetSafeNormal(), Side1.GetSafeNormal()))
			{
				bFalseEdge = false;
			}
			else
			{
				auto ParallelogramCorner = Mesh.VertexPositions[W2] + Side0 + Side1;
				bFalseEdge = (ParallelogramCorner == Mesh.VertexPositions[V2]);
			}
		}
		if (bFalseEdge)
		{
			// -1 == "This is a false adjacency"
			// anything else == "This is not a false adjacency"
			// BUT false adjacency MUST be symmetric; that is if face A is false adjacent to face B,
			// then face B should be false adjacent to face A.
			// We guarantee symmetry even in the presence of slight numerical errors
			FalseAdjacency[Wedge] = 0;
			FalseAdjacency[OtherWedge] = 0;
		}
	}
	return FalseAdjacency;
}

/** Calls the UVAtlas API to generate a new UV map in the given channel.
  * Returns an entirely new Raw Mesh. */
FRawMesh UVAtlasUnwrap(const FRawMesh &Mesh, uint32 dest_uv_channel, uint32 resolution, std::function<int(float)> statusCallback)
{
	TArray<dx::XMFLOAT3> positions;
	for (auto P : Mesh.VertexPositions) { positions.Emplace(P.X, P.Y, P.Z); }

	auto indices = Mesh.WedgeIndices;
	auto indexFormat = DXGI_FORMAT_R32_UINT; // uint32
	auto nFaces = indices.Num() / 3;

	auto maxChartNumber = 0; // as many as needed
	auto maxStretch = 0.3f;

	auto width = resolution; // pixel size of the resulting UV. Affects spacing when atlas is packed
	auto height = resolution;
	float gutter = 1.0f;

	TArray<uint32> Adjacency;
	Adjacency.SetNumZeroed(nFaces * 3);
	auto result = GenerateAdjacencyAndPointReps(
		indices.GetData(), nFaces,
		positions.GetData(), positions.Num(),
		THRESH_POINTS_ARE_SAME,
		nullptr, // we don't need pointRep
		Adjacency.GetData()
	);
	check(SUCCEEDED(result));

	TArray<uint32> FalseAdjacency = QuadFalseAdjacency(Mesh, Adjacency);

	check(Adjacency.Num() == FalseAdjacency.Num());

	auto callbackFrequency = 0.2f;
	auto options = dx::UVATLAS_IMT_DEFAULT;
	std::vector<dx::UVAtlasVertex> vMeshOutVertexBuffer;
	std::vector<uint8_t> vMeshOutIndexBuffer;
	std::vector<uint32_t> pvFacePartitioning(nFaces);
	std::vector<uint32_t> pvVertexRemapArray(indices.Num()); // tells which vertex each new vertex is copied from.

	float maxStretchOut = 0.f;
	size_t numChartsOut = 0;

	auto result2 = UVAtlasCreate(
		positions.GetData(), positions.Num(),
		indices.GetData(), indexFormat,
		nFaces,
		maxChartNumber,
		maxStretch,
		width, height, gutter,
		Adjacency.GetData(),
		FalseAdjacency.GetData(),
		nullptr, // IMT array
		statusCallback,
		callbackFrequency,
		dx::UVATLAS_GEODESIC_QUALITY, // Fast gives pretty bad results, so 
		vMeshOutVertexBuffer,
		vMeshOutIndexBuffer, // these are not actually uint8's. They're probably uint32s
		&pvFacePartitioning,
		&pvVertexRemapArray,
		&maxStretchOut,
		&numChartsOut
	);
	check(SUCCEEDED(result2));

	TArray<uint32> MeshOutIndexBuffer;
	// I know reinterpret_cast's are evil. This one is on purpose
	MeshOutIndexBuffer.Append(reinterpret_cast<uint32 *>(vMeshOutIndexBuffer.data()), vMeshOutIndexBuffer.size() / 4);

	FRawMesh NewMesh;

	check(MeshOutIndexBuffer.Num() == Mesh.WedgeIndices.Num());
	for (auto i = 0; i < MeshOutIndexBuffer.Num(); i++)
	{
		auto p1 = Mesh.GetWedgePosition(i);
		auto p2 = vMeshOutVertexBuffer[MeshOutIndexBuffer[i]].pos;
		check(p1.X == p2.x && p1.Y == p2.y && p1.Z == p2.z);
	}

	NewMesh.FaceMaterialIndices = Mesh.FaceMaterialIndices;
	NewMesh.FaceSmoothingMasks = Mesh.FaceSmoothingMasks;

	// this may SPLIT vertices, but we don't need to split vertices since unreal UVs are per wedge, not per vertex
	NewMesh.VertexPositions = Mesh.VertexPositions;

	NewMesh.WedgeTangentX = Mesh.WedgeTangentX;
	NewMesh.WedgeTangentY = Mesh.WedgeTangentY;
	NewMesh.WedgeTangentZ = Mesh.WedgeTangentZ;

	for (auto uv_channel = 0; uv_channel < MAX_MESH_TEXTURE_COORDS; uv_channel++)
	{
		if (uv_channel != dest_uv_channel)
		{
			NewMesh.WedgeTexCoords[uv_channel] = Mesh.WedgeTexCoords[uv_channel];
		}
		else
		{
			for (auto newWedge : MeshOutIndexBuffer)
			{
				auto uv = vMeshOutVertexBuffer[newWedge].uv;
				NewMesh.WedgeTexCoords[uv_channel].Add({ uv.x,uv.y });
			}
		}
	}

	NewMesh.WedgeIndices = Mesh.WedgeIndices;
	NewMesh.WedgeColors = Mesh.WedgeColors;

	return NewMesh;
}