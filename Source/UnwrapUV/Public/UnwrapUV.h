// Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "Subsystems/AssetEditorSubsystem.h"


class FUnwrapUVModule : public IModuleInterface
{
public:
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/** Unwrap the currently open static mesh in the static mesh editor */
	void PluginButtonClickedFromStaticMeshEditor(class IStaticMeshEditor *);

private:
	/** Add the Unwrap button to the AssetEditor, if it is a StaticMeshEditor */
	void HandleAssetOpenedInEditor(UObject * Asset, class IAssetEditorInstance *Editor);
	/** Adds the Unwrap button to the toolbar of the static mesh editor. */
	void AddStaticMeshEditorToolbarExtension(class FToolBarBuilder & Builder);
};