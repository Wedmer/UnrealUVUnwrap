// Copyright 2017 Digi Labs (http://digilabs.io). All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "UnwrapUVStyle.h"

class FUnwrapUVCommands : public TCommands<FUnwrapUVCommands>
{
public:

	FUnwrapUVCommands()
		: TCommands<FUnwrapUVCommands>(TEXT("UnwrapUV"), NSLOCTEXT("Contexts", "UnwrapUV", "UnwrapUV Plugin"), NAME_None, FUnwrapUVStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > UnwrapMeshFromStaticMeshEditor;

};
